FROM geho/ubuntu-qt:14.04-5.3.2
RUN apt-get update && apt-get install -y \
	build-essential \
	cmake \
	git \
	glib2.0-dev \
	libgl1-mesa-dev \
	libusb-1.0.0-dev \
	nodejs \
	npm \
	python \
	unzip
RUN ln -s /usr/bin/nodejs /usr/bin/node
ENV QTDIR=/opt/Qt5.3.2/5.3/gcc_64

